# What Is It?

JIRA Plugin for graphical charts and graphs.

# Who Owns It?

JIRA team is responsible for doing releases, fixing bugs, etc.

# Resources
* Ask team in JIRA dev room on HipChat.

# Issues
Project is tracked at https://ecosystem.atlassian.net/projects/JCHART

# Running
This project uses Maven 3 and therefore AMPS 5.x. For development purposes you can run it with `atlas-debug` command.

# Releases
Use the atlassian_jira_6_4_branch for 6.4.x / 6.5.x compatible releases.  These should be the 1.x version of this plugin.
Use the atlassian_jira_7_2_branch for JIRA 7.x and JDK 8 compatible releases.  These should be the 2.x version of this plugin.
The master branch of this plugin is targeting JIRA 8.x and is JDK 8 only. These should be the 3.x version of this plugin.
