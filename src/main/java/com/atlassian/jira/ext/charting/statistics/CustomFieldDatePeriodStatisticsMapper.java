package com.atlassian.jira.ext.charting.statistics;

import com.atlassian.jira.issue.search.SearchRequest;
import com.atlassian.jira.issue.statistics.DatePeriodStatisticsMapper;
import com.atlassian.jira.jql.builder.JqlQueryBuilder;
import org.jfree.data.time.TimePeriod;

import java.util.Date;


public class CustomFieldDatePeriodStatisticsMapper extends DatePeriodStatisticsMapper
{
    private final String customFieldId;

    public CustomFieldDatePeriodStatisticsMapper(Class timePeriodClass, String documentConstant)
    {
        super(null, documentConstant, null);
        customFieldId = documentConstant.substring("customfield_".length());
    }

    @Override
    public SearchRequest getSearchUrlSuffix(TimePeriod timePeriod, SearchRequest searchRequest)
    {
        // Copied from parent class, with some mods.
        Date startDate = timePeriod.getStart();
        Date endDate = new Date(timePeriod.getEnd().getTime());

        JqlQueryBuilder builder = JqlQueryBuilder.newBuilder(searchRequest.getQuery());
        builder.where().defaultAnd().addDateRangeCondition(new StringBuilder("cf[").append(customFieldId).append("]").toString(), startDate, endDate);

        return new SearchRequest(builder.buildQuery(), searchRequest.getOwnerUserName(), null, null);
    }
}
