package com.atlassian.jira.ext.charting.data;

import com.atlassian.core.util.DateUtils;
import com.atlassian.jira.charts.util.LuceneDateUtils;
import com.atlassian.jira.ext.charting.field.TimeInStatusDAO;
import com.atlassian.jira.issue.statistics.util.FieldDocumentHitCollector;
import com.google.common.collect.Sets;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexableField;
import org.jfree.data.time.RegularTimePeriod;

import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.TimeZone;

/**
 * A hit collector that will build the average time spent in a state given the custom field id which
 * has stored the information.
 */
public class AverageTimeInStatusHitCollector extends FieldDocumentHitCollector
{


    private final String resolutionDateConstant;
    private final String averageTimeInStatusConstant;
    private final String status;
    private final Map<RegularTimePeriod, Double> totalTimes;
    private final Map<RegularTimePeriod, Double> totalCounts;
    private final Class timePeriodClass;
    private final int days;
    private final boolean numberTimesInStatus;
    private final TimeZone userTimeZone;
    private final Calendar calendar;

    public AverageTimeInStatusHitCollector(String resolutionDateConstant, String averageTimeInStatusConstant, String status, Map<RegularTimePeriod, Double> totalTime, Map<RegularTimePeriod, Double> totalCount, Class timePeriodClass, int days, boolean numberTimesInStatus, TimeZone userTimeZone) {
        super();
        this.resolutionDateConstant = resolutionDateConstant;
        this.averageTimeInStatusConstant = averageTimeInStatusConstant;
        this.status = status;
        this.totalTimes = totalTime;
        this.totalCounts = totalCount;
        this.timePeriodClass = timePeriodClass;
        this.days = days;
        this.numberTimesInStatus = numberTimesInStatus;
        this.userTimeZone = userTimeZone;
        this.calendar = Calendar.getInstance(userTimeZone);
    }

    @Override
    protected Set<String> getFieldsToLoad() {
        return Collections.unmodifiableSet(Sets.newHashSet(resolutionDateConstant, averageTimeInStatusConstant));
    }

    @Override
    public void collect(Document d)
    {
        Double amountOfTimeSpent = null;
        String amountOfTimeString = d.get(averageTimeInStatusConstant);
        Date resolutionDate = null;
        IndexableField resolutionDateStr = d.getField(resolutionDateConstant);
        if (resolutionDateStr != null)
        {
            resolutionDate = LuceneDateUtils.indexableFieldToDate(resolutionDateStr);
        }

        if (amountOfTimeString != null)
        {
            if (numberTimesInStatus)
            {
                amountOfTimeSpent = getNumberOfTimesInStatus(amountOfTimeString);
            }
            else
            {
                amountOfTimeSpent = getSecondsInStatus(amountOfTimeString);
            }
        }

        // find earliest date, then move it forwards until we hit now
        RegularTimePeriod cursor = RegularTimePeriod.createInstance(timePeriodClass, new Date(System.currentTimeMillis() - days * DateUtils.DAY_MILLIS), userTimeZone);
        RegularTimePeriod now = RegularTimePeriod.createInstance(timePeriodClass, new Date(), userTimeZone);

        while (cursor != null && cursor.compareTo(now) < 0)
        {
            processCursor(cursor, amountOfTimeSpent, resolutionDate);
            cursor = cursor.next();
            cursor.peg(calendar);
        }
        processCursor(now, amountOfTimeSpent, resolutionDate); /* Why are we doing this ? */
    }

// Setter/getter need not to be calculated
///CLOVER:OFF
    protected Double getSecondsInStatus(String amountOfTimeString) {
        return TimeInStatusDAO.getSecondsInStatus(status, amountOfTimeString);
    }

    protected Double getNumberOfTimesInStatus(String amountOfTimeString) {
        return TimeInStatusDAO.getNumberOfTimesInStatus(status, amountOfTimeString);
    }
///CLOVER:ON

    private void processCursor(RegularTimePeriod cursor, Double amountOfTimeSpent, Date resolutionDate)
    {
        double totalTime = getDouble(totalTimes, cursor);
        double totalCount = getDouble(totalCounts, cursor);

        long cursorStart = cursor.getFirstMillisecond();
        long cursorEnd = cursor.getLastMillisecond();


        if (amountOfTimeSpent != null && (resolutionDate == null || (resolutionDate.getTime() >= cursorStart && resolutionDate.getTime() <= cursorEnd)))
        {
            totalTime += amountOfTimeSpent.longValue();
            totalCount++;
        }

        totalTimes.put(cursor, totalTime);
        totalCounts.put(cursor, totalCount);
    }

    private double getDouble(Map totalTimes, RegularTimePeriod cursor)
    {
        Double value = (Double) totalTimes.get(cursor);
        if (value != null)
        {
            return value;
        }
        return 0D;
    }

}
