package com.atlassian.jira.ext.charting.searchers;

import com.atlassian.jira.issue.customfields.searchers.TextSearcher;
import com.atlassian.jira.issue.customfields.searchers.transformer.CustomFieldInputHelper;
import com.atlassian.jira.jql.operand.JqlOperandResolver;
import com.atlassian.jira.web.FieldVisibilityManager;

/**
 * This class is here because JIRA may have problems loading built-in searchers in v2 plugin context.
 */
public class ChartingPluginTextSearcher extends TextSearcher
{
    public ChartingPluginTextSearcher(FieldVisibilityManager fieldVisibilityManager, JqlOperandResolver jqlOperandResolver, CustomFieldInputHelper customFieldInputHelper)
    {
        super(fieldVisibilityManager, jqlOperandResolver, customFieldInputHelper);
    }


}
